DROP TABLE `table42`;

CREATE TABLE `table42` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `Name` varchar(255) default NULL,
  `Email` varchar(255) default NULL,
  `Address` varchar(255) default NULL,
  `City` varchar(255),
  `Zipcode` varchar(255),
  `Birthdate` varchar(255),
  `GUID` mediumint default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `table42` (`Name`,`Email`,`Address`,`City`,`Zipcode`,`Birthdate`,`GUID`) VALUES ("Cooper Waller","eleifend.vitae.erat@tristique.co.uk","2891 Nec Avenue","Bègles","82110","Jun 15, 2018",232),("Graham Nolan","accumsan.convallis.ante@facilisisnon.com","991-8169 Eu Av.","Hofheim am Taunus","30266","Jun 20, 2016",632),("Bernard Stein","ut.pharetra.sed@Morbivehicula.com","2698 Pede. Av.","Cognelee","33446","Jul 24, 2017",169),("Peter May","turpis.vitae.purus@magnaDuisdignissim.edu","P.O. Box 823, 9336 Malesuada St.","Lugo","42161","Apr 8, 2017",269),("Drew Ellis","consectetuer.adipiscing@enimcommodo.org","2683 Dui, St.","Bansberia","47093","Nov 1, 2016",278),("Laith Harvey","erat.vitae@ut.net","Ap #884-6943 Semper Av.","Emines","18067","Oct 21, 2016",733),("Neil Coleman","sociis@Nullamsuscipit.edu","Ap #229-4281 Ornare, Road","Rüsselsheim","63661","May 28, 2017",591),("Jared Heath","dictum.mi.ac@nonlobortis.net","3953 A, Av.","Püttlingen","46947","Jan 14, 2018",504),("Rajah Marshall","magna@Aeneanegestas.com","P.O. Box 480, 8548 Magna. Ave","Tillicoultry","00001","Dec 6, 2017",718),("Garth Mayo","eget.laoreet@id.edu","P.O. Box 640, 8632 Id St.","Paupisi","63267","Mar 23, 2018",281),("Elmo Owen","ante.lectus@sedorci.net","290-2053 Donec Avenue","Salt Spring Island","75980","Jun 6, 2018",731),("Acton Murray","vel.arcu@semmagnanec.edu","995-5317 Mauris Avenue","Saint-Dié-des-Vosges","12356","Sep 21, 2017",356),("Cadman Hays","purus.ac.tellus@nisisemsemper.net","Ap #198-2585 Erat Ave","Barrhead","34273","Jun 4, 2018",455),("Colby Shelton","et.libero.Proin@lobortis.org","5481 In St.","Macon","45110","Apr 21, 2017",195),("Alvin Goodwin","elementum.purus@Pellentesque.net","P.O. Box 828, 3362 Mus. Road","Bhuj","77583","Dec 26, 2017",349),("Clark Bruce","leo@eratin.org","Ap #977-3179 Non Rd.","Anchorage","60067","Sep 4, 2017",45);
