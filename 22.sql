DROP TABLE `table22`;

CREATE TABLE `table22` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `Name` varchar(255) default NULL,
  `Email` varchar(255) default NULL,
  `Address` varchar(255) default NULL,
  `City` varchar(255),
  `Zipcode` varchar(255),
  `Birthdate` varchar(255),
  `GUID` mediumint default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `table22` (`Name`,`Email`,`Address`,`City`,`Zipcode`,`Birthdate`,`GUID`) VALUES ("Lamar Fleming","litora@dolordapibusgravida.edu","910-5861 Donec Rd.","Tramatza","83195","Aug 30, 2017",365),("Calvin Walls","vulputate@commodo.co.uk","P.O. Box 282, 2128 A, Road","Meduno","50707","Jun 6, 2018",729),("Yoshio Acevedo","eget@gravida.ca","232 Sagittis Rd.","Opprebais","39347","Mar 8, 2018",70),("Quinlan Langley","ac@sit.com","Ap #692-6828 Vivamus Rd.","Orai","84585","Jun 27, 2017",437);
