//STEP 1. Import required packages
import java.sql.*;
import java.util.concurrent.TimeUnit;

public class joinSQL {
	// JDBC driver name and database URL
	String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static String DB_URL = "jdbc:mysql://localhost/joinSQL";
	static int REPEAT = 1;
	// Database credentials
	static final String USER = "newuser";
	static final String PASS = "newuser";

	public static void innerJoin() throws InterruptedException{
		Connection conn = null;
		Statement stmt = null;
		int iters = 16;
		int size = 4;
		for (int i = 1; i < iters; i++) {
			try {
				// STEP 2: Register JDBC driver
				Class.forName("com.mysql.jdbc.Driver");

				// STEP 3: Open a connection
				//System.out.println("Connecting to database...");
				String table1 = "table" + Integer.toString(i+1) + Integer.toString(1) ;
				String table2 = "table" + Integer.toString(i+1) + Integer.toString(2);
				String DB_URL_iter = DB_URL;
				conn = DriverManager.getConnection(DB_URL_iter, USER, PASS);
				// TABLE is "information_schema" + iter. information_schema1, information_schema2, information_schema3
				// size of each table is 4 << (i -1), 4, 8, 16, 32...
				// Tables are information_schema1, information_schema11


				long startTime = System.currentTimeMillis();
				// STEP 4: Execute a join
				//System.out.println("Creating statement...");
				int rowcount = 0;
				ResultSet rs = null;
				for (int repeat = 0; repeat < REPEAT; repeat++) {

                    stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE, //or ResultSet.TYPE_FORWARD_ONLY
                    ResultSet.CONCUR_READ_ONLY);
					String sql = "SELECT * FROM " + table1 + " inner join " + table2 + " on " + table1 +".GUID = " + table2 + ".GUID";
					stmt.executeQuery(sql);
					rowcount = 0;
					//if (rs.last()) {
					//  rowcount = rs.getRow();
					//  rs.beforeFirst(); // not rs.first() because the rs.next() below will move on, missing the first element
					//}
				}
				// STEP 5: Extract data from result set
									// Display values
				long stopTime = System.currentTimeMillis();
				System.out.println((stopTime - startTime)+ ", " + size + ", " + rowcount);
				size = size << 1;
				// STEP 6: Clean-up environment
				//rs.close();
				stmt.close();
				conn.close();
			} catch (SQLException se) {
				// Handle errors for JDBC
				se.printStackTrace();
			} catch (Exception e) {
				// Handle errors for Class.forName
				e.printStackTrace();
			} finally {
				// finally block used to close resources
				try {
					if (stmt != null)
						stmt.close();
				} catch (SQLException se2) {
				}// nothing we can do
				try {
					if (conn != null)
						conn.close();
				} catch (SQLException se) {
					se.printStackTrace();
				}// end finally try
			}// end try
			TimeUnit.SECONDS.sleep(1);
		}// end main
	}

	public static void OuterJoin(boolean left, boolean full) throws InterruptedException{
		Connection conn = null;
		Statement stmt = null;
		int iters = 16;
		int size = 4;
		for (int i = 1; i < iters; i++) {
			try {
				// STEP 2: Register JDBC driver
				Class.forName("com.mysql.jdbc.Driver");

				// STEP 3: Open a connection
				//System.out.println("Connecting to database...");
				String table1 = "table" + Integer.toString(i+1) + Integer.toString(1) ;
				String table2 = "table" + Integer.toString(i+1) + Integer.toString(2);
				String DB_URL_iter = DB_URL;
				conn = DriverManager.getConnection(DB_URL_iter, USER, PASS);
				// TABLE is "information_schema" + iter. information_schema1, information_schema2, information_schema3
				// size of each table is 4 << (i -1), 4, 8, 16, 32...
				// Tables are information_schema1, information_schema11

				long startTime = System.currentTimeMillis();
				int rowcount = 0;
				ResultSet rs = null;
				for (int repeat = 0; repeat < REPEAT; repeat++) {
				// STEP 4: Execute a join
				//System.out.println("Creating statement...");
				stmt = conn.createStatement(
                   ResultSet.TYPE_SCROLL_INSENSITIVE, //or ResultSet.TYPE_FORWARD_ONLY
                   ResultSet.CONCUR_READ_ONLY);
					String sql;
					if(left){
						sql = "SELECT * FROM " + table1 + " left join " + table2 + " on " + table1 +".GUID = " + table2 + ".GUID";
					} else if (!full) {
						sql = "SELECT * FROM " + table1 + " right join " + table2 + " on " + table1 +".GUID = " + table2 + ".GUID";
					} else {
						sql = "SELECT * FROM " + table1 + " left join " + table2 + " on " + table1 +".GUID = " + table2 + ".GUID "
                        + "UNION " + "SELECT * FROM " + table1 + " right join " + table2 + " on " + table1 +".GUID = " + table2 + ".GUID";
					}

					stmt.executeQuery(sql);
					rowcount = 0;
					//if (rs.last()) {
					//  rowcount = rs.getRow();
					//  rs.beforeFirst(); // not rs.first() because the rs.next() below will move on, missing the first element
					//}
				}
				// maybe traverse through the table to count rows? but implications.
				long stopTime = System.currentTimeMillis();
				System.out.println((stopTime - startTime)+ ", " + size + ", " + rowcount);
				size = size << 1;
				// STEP 6: Clean-up environment
				//rs.close();
				stmt.close();
				conn.close();
			} catch (SQLException se) {
				// Handle errors for JDBC
				se.printStackTrace();
			} catch (Exception e) {
				// Handle errors for Class.forName
				e.printStackTrace();
			} finally {
				// finally block used to close resources
				try {
					if (stmt != null)
						stmt.close();
				} catch (SQLException se2) {
				}// nothing we can do
				try {
					if (conn != null)
						conn.close();
				} catch (SQLException se) {
					se.printStackTrace();
				}// end finally try
			}// end try
			TimeUnit.SECONDS.sleep(1);
		}// end main
	}

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Inner:");
		System.out.println("Time (ms), Table size");
		innerJoin();
		System.out.println("Left:");
		OuterJoin(true, false); //left
		System.out.println("Right:");
		OuterJoin(false, false); // right
		System.out.println("Full Outer:");
		OuterJoin(false, true); // full outer
	}
}// end FirstExample
