#include <stdio.h>
#include <time.h>
#include <iostream>
#include <cstdlib>
#include <unordered_map>
#include <algorithm>
#include <random>       // std::default_random_engine
#include <chrono>
using namespace std;


//nested loop join: right table no index scan
//Merge join X
//Hash join

typedef struct{
    int key;
    int content;
}element;

element **tables;
element *outTableNV;

element *outTableSQL;
int numTables;
int sizeTable_1, sizeTable_2;
int readCountNV, readCountSQL;
int writeCountNV, writeCountSQL;

int colTable1, colTable2;

int searchInTable2(int val){
    for (size_t i = 0; i < searchInTable2; i++) {
        readCountSQL++;
        if (tables[1][i].key == val) {
            return tables[1][i].content;
        }
    }
    return -1;
}

void joinNV(){
    int count = 0;
    for (size_t i = 0; i < sizeTable_1; i++) {
        readCountNV++;
        int val = searchInTable2(tables[0][i].key);
        if(val != -1)
        {
            readCountNV++;
            outTableNV[count++] = (tables[0][i] & colTable1) + (val & colTable2);
            writeCountNV++;
        }
    }
}

void joinSQL(){
    readCountSQL = writeCountSQL = 0;
    int count = 0;
    for (size_t i = 0; i < sizeTable_1; i++) {
        readCountSQL++;
        int val = searchInTable2(tables[0][i]);
        if(val != -1)
        {
            outTableSQL[count++] = (tables[0][i] & colTable1) + (val & colTable2);
            writeCountSQL++;
        }
    }
}

bool check(){
    for (size_t i = 0; i < sizeTable; i++) {
        if(outTableNV[i].content!=outTableSQL[i].content)
            return false;
    }
    return true;
}

void initialize(){
    for (size_t i = 0; i < 1; i++) {
        for (size_t j = 0; j < sizeTable_1; j++) {
            tables[i][j].key = j;
            tables[i][j].content = rand()%sizeTable_2;
        }
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        shuffle (tables[i], tables[i] + sizeTable_1, std::default_random_engine(seed));
    }
    for (size_t j = 0; j < sizeTable_2; j++) {
        tables[i][j].key = j;
        tables[i][j].content = rand()%sizeTable_2;
    }
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    shuffle (tables[i], tables[i] + sizeTable_2, std::default_random_engine(seed));
}

int main(int argc, char const *argv[]) {
    srand (time(NULL));
    readCountNV = writeCountNV = readCountSQL = writeCountSQL = 0;
    numTables = atoi(argv[1]);
    sizeTable = atoi(argv[2]);
    colTable1 = atoi(argv[3]);
    colTable2 = atoi(argv[4]);
    std::cout << "numTables: "<<numTables<<"\tsizeTable: "<<sizeTable << '\n';
    tables = (element **)malloc(numTables*sizeof(element *));
    outTableNV = (element *)malloc(sizeTable*sizeof(element));
    outTableSQL = (element *)malloc(sizeTable*sizeof(element));
    for (size_t i = 0; i < numTables; i++) {
        tables[i] = (element *)malloc(sizeTable*sizeof(element));
    }
    //TODO: populate table via random function
    initialize();
    //implement below 2 functions

    joinNV();
    joinSQL();

    if(check()){
        std::cout << "OK!" << '\n';
        std::cout << "readCountNV: "<<readCountNV<<"\twriteCountNV: "<<writeCountNV << '\n';
        std::cout << "readCountSQL: "<<readCountNV<<"\twriteCountSQL: "<<writeCountNV << '\n';
    }
    else{
        std::cout << "NOT OK!" << '\n';
    }

    for (size_t i = 0; i < numTables; i++) {
        free(tables[i]);
    }
    free(tables);
    free(outTableSQL);
    free(outTableNV);
    return 0;
}
