DROP TABLE `table32`;

CREATE TABLE `table32` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `Name` varchar(255) default NULL,
  `Email` varchar(255) default NULL,
  `Address` varchar(255) default NULL,
  `City` varchar(255),
  `Zipcode` varchar(255),
  `Birthdate` varchar(255),
  `GUID` mediumint default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `table32` (`Name`,`Email`,`Address`,`City`,`Zipcode`,`Birthdate`,`GUID`) VALUES ("Ahmed Good","Integer.vulputate@Inatpede.ca","2395 Egestas St.","Penrith","50010","Aug 11, 2016",797),("Arthur Cervantes","cursus.et.magna@velsapien.com","P.O. Box 489, 3073 Eu Ave","Wierde","93842","Jan 24, 2018",405),("Walter Todd","purus@euodioPhasellus.org","7066 Luctus Rd.","Rutigliano","37308","Jan 30, 2018",401),("Buckminster Cruz","rutrum@semper.net","4519 Est. St.","Cirencester","28020","May 23, 2018",242),("Wing Pugh","massa.Integer.vitae@NulladignissimMaecenas.net","2101 Nascetur Rd.","Parbhani","50854","Nov 16, 2017",395),("Alan Cantrell","iaculis@vitaesodalesnisi.ca","547-3734 Nonummy Street","Avise","57783","Jun 15, 2016",359),("Judah Drake","at@magnis.org","3623 Ante Rd.","Meldert","27342","Feb 28, 2017",673),("Uriah Blevins","laoreet.posuere@semegestasblandit.com","968-6023 Sagittis. St.","Nässjö","53507","Jul 22, 2016",508);
