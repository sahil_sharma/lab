DROP TABLE `table21`;

CREATE TABLE `table21` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `Name` varchar(255) default NULL,
  `Email` varchar(255) default NULL,
  `Address` varchar(255) default NULL,
  `City` varchar(255),
  `Zipcode` varchar(255),
  `Birthdate` varchar(255),
  `GUID` mediumint default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `table21` (`Name`,`Email`,`Address`,`City`,`Zipcode`,`Birthdate`,`GUID`) VALUES ("Steven Greer","mauris.Suspendisse.aliquet@diamnunc.net","P.O. Box 284, 1266 Egestas. Avenue","Natales","37453","Jun 12, 2017",546),("Erasmus Booth","risus.odio.auctor@adipiscing.ca","2708 In Avenue","Rocca di Cambio","43950","Jan 10, 2017",541),("Kenneth Green","sem@est.com","578-1487 Molestie Street","Sant'Onofrio","43409","Jan 13, 2018",401),("Jordan Mitchell","nisi@et.ca","8255 Odio. Av.","Beauvais","76086","Apr 29, 2018",461);
