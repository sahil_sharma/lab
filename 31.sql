DROP TABLE `table31`;

CREATE TABLE `table31` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `Name` varchar(255) default NULL,
  `Email` varchar(255) default NULL,
  `Address` varchar(255) default NULL,
  `City` varchar(255),
  `Zipcode` varchar(255),
  `Birthdate` varchar(255),
  `GUID` mediumint default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `table31` (`Name`,`Email`,`Address`,`City`,`Zipcode`,`Birthdate`,`GUID`) VALUES ("Seth Cameron","et.libero.Proin@magnaPhasellus.co.uk","P.O. Box 427, 2980 In Avenue","Belgrade","27924","Sep 14, 2017",345),("Hu Phelps","Fusce.dolor@ullamcorpervelit.net","635-2210 Id, Ave","Kapiti","92543","Mar 10, 2018",499),("Theodore Bruce","in@senectusetnetus.ca","Ap #573-8885 Varius St.","Clovenfords","34117","Jan 21, 2017",598),("Seth Palmer","dui.quis@faucibusorciluctus.net","Ap #397-4803 Enim. Avenue","Dresden","70836","Nov 5, 2016",69),("Quinn Duffy","eget.massa@nisiaodio.ca","482-8855 Ipsum St.","Seattle","88828","Jan 6, 2017",782),("Timon Tran","dolor.sit.amet@porttitortellus.com","Ap #785-2816 Volutpat Road","Heerenveen","05067","May 18, 2018",174),("Holmes Cox","enim.nisl@est.org","Ap #784-3694 Enim. Ave","Aserrí","78204","Nov 2, 2017",576),("Castor Becker","aliquet.nec@duiCumsociis.ca","P.O. Box 849, 9652 Per St.","Deutschkreutz","22070","Nov 9, 2017",584);
