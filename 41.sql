DROP TABLE `table41`;

CREATE TABLE `table41` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `Name` varchar(255) default NULL,
  `Email` varchar(255) default NULL,
  `Address` varchar(255) default NULL,
  `City` varchar(255),
  `Zipcode` varchar(255),
  `Birthdate` varchar(255),
  `GUID` mediumint default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `table41` (`Name`,`Email`,`Address`,`City`,`Zipcode`,`Birthdate`,`GUID`) VALUES ("Peter Shields","neque@estMauriseu.ca","131-4368 Ultricies Rd.","Hindupur","30097","Nov 26, 2017",582),("Jakeem Buchanan","Cum.sociis@mienim.net","2299 Justo Rd.","Monte San Pietrangeli","87423","Mar 18, 2017",280),("Brock Flowers","euismod@mollisInteger.ca","559-4042 Dictum Ave","Rosciano","04364","Oct 10, 2016",547),("Beck Jensen","Nullam.suscipit@quam.co.uk","5600 Aliquam Rd.","Saint-Maur-des-Fossés","04753","Oct 1, 2017",270),("Brock Lawson","venenatis@interdumlibero.co.uk","Ap #442-1969 Parturient Road","Aydın","09437","Mar 6, 2017",221),("Len Reyes","ullamcorper@id.com","P.O. Box 393, 7121 At, Rd.","Heidelberg","73751","Jun 2, 2018",111),("Abdul Compton","massa.Mauris@cubilia.org","P.O. Box 909, 6921 Fermentum Street","Ruette","02526","Nov 18, 2016",479),("Armand Curtis","turpis.nec.mauris@perinceptos.ca","Ap #267-3193 Elit. Ave","Ollolai","90299","Jan 9, 2017",43),("Reuben Rivers","pede.Cum@convallis.net","Ap #627-9252 Vitae, St.","Chiavari","97235","Dec 8, 2016",251),("Elliott Kirby","ultrices.posuere@luctus.org","2349 In, Ave","Coreglia Antelminelli","23249","Nov 26, 2016",665),("Patrick Burks","magna.Sed@odioapurus.ca","6047 Dui. Road","Haridwar","55178","Jun 13, 2018",519),("Orson Mason","ante.ipsum@Duismienim.com","Ap #820-5092 Non Rd.","Oakham","09171","Jan 16, 2018",345),("Jin Bradshaw","mauris.sapien.cursus@metusvitae.com","9655 Nec Avenue","Sint-Gillis-Waas","74493","Mar 5, 2018",531),("Zephania French","nonummy@Suspendisse.net","P.O. Box 311, 4301 Sed St.","Haverhill","39578","Apr 1, 2018",596),("Jonah Henson","Cras@imperdietornare.edu","P.O. Box 322, 6849 Duis Rd.","Vauda Canavese","22316","May 5, 2018",343),("Malik Conner","tincidunt@Donectempus.co.uk","4874 Tincidunt Street","Bosa","80029","Dec 28, 2017",144);
